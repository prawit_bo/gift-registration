import 'package:flutter/material.dart';
import 'package:gift_registration_2/constants.dart';
import 'package:gift_registration_2/size_config.dart';

class DefaultButton extends StatelessWidget {
  final String text, disabledText;
  final Function press;
  final bool isButtonDisabled;

  const DefaultButton({
    Key key,
    @required this.text,
    @required this.press,
    this.disabledText,
    this.isButtonDisabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: getProportionateScreenHeight(56),
      child: TextButton(
        onPressed: isButtonDisabled ? null : press,
        child: Text(
          isButtonDisabled ? (disabledText ?? text) : text,
          style: TextStyle(color: Colors.white),
        ),
        style: TextButton.styleFrom(
          primary: Colors.white,
          backgroundColor: isButtonDisabled ? Color(0xFF8B8B8B) : kPrimaryColor,
          textStyle: TextStyle(
            fontSize: getProportionateScreenWidth(18),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
    );
  }
}
