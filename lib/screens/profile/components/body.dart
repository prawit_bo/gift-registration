import 'package:flutter/material.dart';
import 'package:gift_registration_2/screens/dashboard/dashboard_screen.dart';
import 'package:gift_registration_2/screens/profile/components/profile_menu.dart';
import 'package:gift_registration_2/screens/profile/components/profile_pic.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ProfilePic(),
          SizedBox(height: 10),
          Text("John Doe", style: TextStyle(fontSize: 16, color: Colors.black),),
          SizedBox(height: 20),
          ProfileMenu(
            text: "Dashboard",
            icon: "assets/icons/Gift Icon.svg",
            press: () =>
                Navigator.pushNamed(context, DashboardScreen.routeName),
          ),
          ProfileMenu(
            text: "Notifications",
            icon: "assets/icons/Bell.svg",
            press: () {},
          ),
          ProfileMenu(
            text: "Help Center",
            icon: "assets/icons/Question mark.svg",
            press: () {},
          ),
          ProfileMenu(
            text: "Log Out",
            icon: "assets/icons/Log out.svg",
            press: () {},
          ),
        ],
      ),
    );
  }
}
