import 'package:flutter/material.dart';
import 'package:gift_registration_2/components/custom_bottom_nav_bar.dart';
import 'package:gift_registration_2/enums.dart';
import 'package:gift_registration_2/screens/profile/components/body.dart';

class ProfileScreen extends StatelessWidget {
  static String routeName = "/profile";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
      ),
      body: Body(),
      bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.profile),
    );
  }
}
