import 'package:flutter/material.dart';
import 'package:gift_registration_2/components/custom_bottom_nav_bar.dart';
import 'package:gift_registration_2/enums.dart';
import 'package:gift_registration_2/screens/home/components/body.dart';

class HomeScreen extends StatelessWidget {
  static String routeName = "/home";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
      bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.home),
    );
  }
}
