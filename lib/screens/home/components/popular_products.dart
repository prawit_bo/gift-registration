import 'package:flutter/material.dart';
import 'package:gift_registration_2/components/product_card.dart';
import 'package:gift_registration_2/models/Product.dart';
import 'package:gift_registration_2/screens/category/category_screen.dart';
import 'package:gift_registration_2/screens/details/details_screen.dart';
import 'package:gift_registration_2/screens/home/components/section_title.dart';
import 'package:gift_registration_2/size_config.dart';

class PopularProducts extends StatelessWidget {
  const PopularProducts({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SectionTitle(
          text: "Popular Gift",
          press: () => Navigator.pushNamed(context, CategoryScreen.routeName),
        ),
        SizedBox(height: getProportionateScreenWidth(20)),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...List.generate(
                demoProducts.length,
                (index) {
                  if (demoProducts[index].isPopular)
                    return ProductCard(
                      product: demoProducts[index],
                      press: () => Navigator.pushNamed(
                          context, DetailsScreen.routeName,
                          arguments: ProductDetailsArguments(
                              product: demoProducts[index])),
                    );

                  return SizedBox.shrink();
                },
              ),
              SizedBox(width: getProportionateScreenWidth(20)),
            ],
          ),
        ),
      ],
    );
  }
}
