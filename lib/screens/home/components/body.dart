import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gift_registration_2/screens/home/components/categories.dart';
import 'package:gift_registration_2/screens/home/components/discount_banner.dart';
import 'package:gift_registration_2/screens/home/components/home_header.dart';
import 'package:gift_registration_2/screens/home/components/popular_products.dart';
import 'package:gift_registration_2/screens/home/components/special_offers.dart';
import 'package:gift_registration_2/size_config.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: getProportionateScreenWidth(20)),
            HomeHeader(),
            SizedBox(height: getProportionateScreenWidth(10)),
            DiscountBanner(),
            Categories(),
            SizedBox(height: getProportionateScreenWidth(30)),
            SpecialOffers(),
            SizedBox(height: getProportionateScreenWidth(30)),
            PopularProducts(),
            SizedBox(height: getProportionateScreenWidth(30)),
          ],
        ),
      ),
    );
  }
}
