import 'package:flutter/material.dart';
import 'package:gift_registration_2/screens/category/components/body.dart';

class CategoryScreen extends StatelessWidget {
  static String routeName = "/category";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Spa Basket"),
      ),
      body: Body(),
    );
  }
}
