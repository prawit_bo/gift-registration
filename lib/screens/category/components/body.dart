import 'package:flutter/material.dart';
import 'package:gift_registration_2/models/Product.dart';
import 'package:gift_registration_2/screens/category/components/category_item_card.dart';
import 'package:gift_registration_2/screens/details/details_screen.dart';
import 'package:gift_registration_2/size_config.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
      child: ListView.builder(
        itemCount: demoProducts.length,
        itemBuilder: (context, index) => Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: CategoryItemCard(
            product: demoProducts[index],
            press: () => Navigator.pushNamed(
              context,
              DetailsScreen.routeName,
              arguments: ProductDetailsArguments(
                product: demoProducts[index],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
