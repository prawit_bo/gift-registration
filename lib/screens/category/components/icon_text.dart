import 'package:flutter/material.dart';
import 'package:gift_registration_2/constants.dart';
import 'package:gift_registration_2/size_config.dart';

class IconText extends StatelessWidget {
  const IconText({
    Key key,
    @required this.text,
    @required this.icon,
    this.color = kTextColor,
  }) : super(key: key);

  final String text;
  final IconData icon;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: getProportionateScreenWidth(10)),
      child: RichText(
        text: TextSpan(
          children: [
            WidgetSpan(
              child: Icon(
                icon,
                size: 16,
                color: color,
              ),
            ),
            TextSpan(text: text)
          ],
          style: TextStyle(color: color),
        ),
      ),
    );
  }
}
