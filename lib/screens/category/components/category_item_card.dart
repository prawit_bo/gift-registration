import 'package:flutter/material.dart';
import 'package:gift_registration_2/constants.dart';
import 'package:gift_registration_2/models/Product.dart';
import 'package:gift_registration_2/screens/category/components/icon_text.dart';
import 'package:gift_registration_2/size_config.dart';

class CategoryItemCard extends StatelessWidget {
  const CategoryItemCard({
    Key key,
    @required this.product,
    @required this.press,
  }) : super(key: key);

  final Product product;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Color(0xFFF5F6F9),
          borderRadius: BorderRadius.circular(15),
        ),
        child: Row(
          children: [
            SizedBox(
              width: getProportionateScreenWidth(88),
              child: AspectRatio(
                aspectRatio: 0.88,
                child: Image.asset(product.images[0]),
              ),
            ),
            SizedBox(width: getProportionateScreenWidth(10)),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  product.title,
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                  ),
                  maxLines: 2,
                ),
                const SizedBox(height: 10),
                Text(
                  "\$${product.price}",
                  style: TextStyle(
                    color: kPrimaryColor,
                  ),
                ),
                const SizedBox(height: 10),
                Row(
                  children: [
                    IconText(
                      text: "Reserved",
                      icon: Icons.book_online_outlined,
                      color: product.isReserved ? Color(0xFF3B8183) : kTextColor,
                    ),
                    IconText(
                      text: "Wait for Approve",
                      icon: Icons.check_circle_rounded,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
