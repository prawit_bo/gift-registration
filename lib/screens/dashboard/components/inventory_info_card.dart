import 'package:flutter/material.dart';
import 'package:gift_registration_2/models/InventoryInfo.dart';

class InventoryInfoCard extends StatelessWidget {
  const InventoryInfoCard({
    Key key,
    @required this.info,
  }) : super(key: key);

  final InventoryInfo info;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Color(0xFFF6F7F9),
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
              color: info.color.withOpacity(0.3),
              borderRadius: BorderRadius.all(
                Radius.circular(20),
              ),
            ),
            child: Icon(info.icon, color: info.color),
          ),
          Text(
            info.numOfItem.toString(),
            style: TextStyle(fontSize: 30, color: Colors.black),
          ),
          Text(
            info.title,
            style: TextStyle(fontSize: 18),
          ),
        ],
      ),
    );
  }
}
