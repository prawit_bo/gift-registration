import 'package:flutter/material.dart';
import 'package:gift_registration_2/screens/dashboard/components/body.dart';

class DashboardScreen extends StatelessWidget {
  static String routeName = "/dashboard";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
      ),
      body: Body(),
    );
  }
}
