import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gift_registration_2/components/default_button.dart';
import 'package:gift_registration_2/models/Product.dart';
import 'package:gift_registration_2/screens/details/components/product_description.dart';
import 'package:gift_registration_2/screens/details/components/product_images.dart';
import 'package:gift_registration_2/size_config.dart';

class Body extends StatelessWidget {
  final Product product;

  const Body({
    Key key,
    @required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ProductImages(product: product),
          TopRoundedContainer(
            color: Colors.white,
            child: Column(
              children: [
                ProductDescription(
                  product: product,
                  pressOnSeeMore: () {},
                ),
                TopRoundedContainer(
                  color: Color(0xFFF6F7F9),
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: SizeConfig.screenWidth * 0.15,
                      right: SizeConfig.screenWidth * 0.15,
                      top: getProportionateScreenWidth(15),
                      bottom: getProportionateScreenWidth(40),
                    ),
                    child: DefaultButton(
                      text: "Add to Cart",
                      press: () {},
                      isButtonDisabled: product.isReserved ?? false,
                      disabledText: "Reserved",
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
