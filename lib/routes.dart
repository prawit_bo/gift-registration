import 'package:flutter/material.dart';
import 'package:gift_registration_2/screens/cart/cart_screen.dart';
import 'package:gift_registration_2/screens/category/category_screen.dart';
import 'package:gift_registration_2/screens/dashboard/dashboard_screen.dart';
import 'package:gift_registration_2/screens/details/details_screen.dart';
import 'package:gift_registration_2/screens/forgot_password/forgot_password_screen.dart';
import 'package:gift_registration_2/screens/home/home_screen.dart';
import 'package:gift_registration_2/screens/login_success/login_success_screen.dart';
import 'package:gift_registration_2/screens/profile/profile_screen.dart';
import 'package:gift_registration_2/screens/sign_in/sign_in_screen.dart';
import 'package:gift_registration_2/screens/splash/splash_screen.dart';

// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  SignInScreen.routeName: (context) => SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  LoginSuccessScreen.routeName: (context) => LoginSuccessScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  DetailsScreen.routeName: (context) => DetailsScreen(),
  CartScreen.routeName: (context) => CartScreen(),
  ProfileScreen.routeName: (context) => ProfileScreen(),
  CategoryScreen.routeName: (context) => CategoryScreen(),
  DashboardScreen.routeName: (context) => DashboardScreen(),
};
