import 'package:flutter/material.dart';
import 'package:gift_registration_2/constants.dart';

class InventoryInfo {
  final String title;
  final int numOfItem;
  final Color color;
  final IconData icon;

  InventoryInfo({
    this.icon,
    this.title,
    this.numOfItem,
    this.color,
  });
}

List demoInventories = [
  InventoryInfo(
    icon: Icons.receipt,
    title: "Items",
    numOfItem: 44,
    color: kPrimaryColor,
  ),
  InventoryInfo(
    icon: Icons.folder,
    title: "Places",
    numOfItem: 2,
    color: Color(0xFFFFA113),
  ),
  InventoryInfo(
    icon: Icons.auto_awesome_motion,
    title: "Total Quantity",
    numOfItem: 22,
    color: Color(0xFFA4CDFF),
  ),
  InventoryInfo(
    icon: Icons.money,
    title: "Total Value",
    numOfItem: 555,
    color: Color(0xFF007EE5),
  ),
];
