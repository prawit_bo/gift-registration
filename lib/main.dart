import 'package:flutter/material.dart';
import 'package:gift_registration_2/routes.dart';
import 'package:gift_registration_2/screens/splash/splash_screen.dart';
import 'package:gift_registration_2/theme.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Gift Kae',
      theme: theme(),
      // We use routeName so that we don't need to remember the name
      initialRoute: SplashScreen.routeName,
      routes: routes,
    );
  }
}
